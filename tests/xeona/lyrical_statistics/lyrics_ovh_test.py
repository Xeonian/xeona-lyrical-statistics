from contextlib import asynccontextmanager
from typing import Any, Generator
from unittest import mock

import pytest
from aiohttp import ClientResponse, ClientSession

from xeona.lyrical_statistics.lyrics_ovh import get_lyrics_for_work


@pytest.mark.asyncio
async def test_get_lyrics_for_work_calls_correct_url_and_returns_successful_response_body_as_json() -> None:
    response_body_json = mock.Mock()

    response_mock = mock.Mock(ClientResponse)
    response_mock.status = 200
    response_mock.json.return_value = response_body_json

    client_session_mock = mock.Mock(ClientSession)
    client_session_mock.get.return_value = _context_manager_yielding(response_mock)

    assert await get_lyrics_for_work(client_session_mock, 'My favourite artist',
                                     'My favourite song') is response_body_json
    client_session_mock.get.assert_called_once_with("https://api.lyrics.ovh/v1/My favourite artist/My favourite song")


@pytest.mark.asyncio
async def test_get_lyrics_for_work_raises_value_error_on_unsuccessful_response() -> None:
    response_mock = mock.Mock(ClientResponse)
    response_mock.status = 404

    client_session_mock = mock.Mock(ClientSession)
    client_session_mock.get.return_value = _context_manager_yielding(response_mock)

    with pytest.raises(ValueError):
        await get_lyrics_for_work(client_session_mock, 'My favourite artist', 'My favourite song')


@asynccontextmanager
async def _context_manager_yielding(value: Any) -> Generator[Any, None, None]:
    yield value
