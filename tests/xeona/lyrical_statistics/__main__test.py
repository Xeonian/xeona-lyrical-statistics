import uuid
from asyncio.futures import Future
from contextlib import asynccontextmanager
from typing import Any, Generator
from unittest import mock

import pytest
from aiohttp import ClientSession

from xeona.lyrical_statistics.__main__ import determine_artist_id, get_works_by_artist_id, count_lyrics_for_work, \
    print_average_lyric_count_for_artist


@pytest.mark.asyncio
@mock.patch('xeona.lyrical_statistics.__main__.musicbrainz')
async def test_determine_artist_id_queries_musicbrainz_and_picks_first_returned_value(
        musicbrainz_module_mock) -> None:
    artist_id = str(uuid.uuid4())
    musicbrainz_module_mock.get_artists_by_name.return_value = _awaitable_value({
        'artists': [
            {
                'id': artist_id,
            },
            {
                'id': 'other_artist_id'
            }
        ]
    })

    session = mock.Mock(ClientSession)
    artist_name = 'My Favourite Artist'
    assert await determine_artist_id(session, artist_name) == artist_id
    musicbrainz_module_mock.get_artists_by_name.assert_called_once_with(session, artist_name)


@pytest.mark.asyncio
@mock.patch('xeona.lyrical_statistics.__main__.musicbrainz')
async def test_get_works_by_artist_id_returns_all_works_titles_per_page(musicbrainz_module_mock) -> None:
    work_count = 3
    musicbrainz_module_mock.get_works_for_artist.side_effect = [
        _awaitable_value({
            'work-count': work_count,
            'works': [
                {
                    'title': 'First Work'
                },
                {
                    'title': 'Second Work'
                }
            ]
        }),
        _awaitable_value({
            'work-count': work_count,
            'works': [
                {
                    'title': 'Third Work'
                }
            ]
        })
    ]

    session = mock.Mock(ClientSession)
    artist_id = str(uuid.uuid4())
    assert [title async for title in get_works_by_artist_id(session, artist_id)] == \
           ['First Work', 'Second Work', 'Third Work']
    musicbrainz_module_mock.get_works_for_artist.assert_any_call(session, artist_id, offset=0)
    musicbrainz_module_mock.get_works_for_artist.assert_any_call(session, artist_id, offset=2)


@pytest.mark.asyncio
@mock.patch('xeona.lyrical_statistics.__main__.lyrics_ovh')
async def test_count_lyrics_for_work_counts_number_of_words_in_returned_lyrics(lyrics_ovh_module_mock) -> None:
    lyrics_ovh_module_mock.get_lyrics_for_work.return_value = _awaitable_value({
        'lyrics': """
            I am the egg man
            They are the egg men
            I am the walrus
            Goo goo g'joob
        """
    })

    session = mock.Mock(ClientSession)
    artist_name = 'My Favourite Artist'
    work_name = 'My Favourite Song'
    assert await count_lyrics_for_work(session, artist_name, work_name) == 17
    lyrics_ovh_module_mock.get_lyrics_for_work.assert_called_once_with(session, artist_name, work_name)


@pytest.mark.asyncio
@mock.patch('xeona.lyrical_statistics.__main__.lyrics_ovh')
async def test_count_lyrics_for_work_returns_none_when_lyrics_not_found(lyrics_ovh_module_mock) -> None:
    lyrics_ovh_module_mock.get_lyrics_for_work.side_effect = ValueError

    assert await count_lyrics_for_work(mock.Mock(ClientSession), 'My Favourite Artist', 'My Favourite Song') is None


@pytest.mark.asyncio
@mock.patch('builtins.print')
@mock.patch('xeona.lyrical_statistics.__main__.count_lyrics_for_work')
@mock.patch('xeona.lyrical_statistics.__main__.get_works_by_artist_id')
@mock.patch('xeona.lyrical_statistics.__main__.determine_artist_id')
@mock.patch('xeona.lyrical_statistics.__main__.ClientSession')
async def test_prints_average_lyric_count_ignores_missing(
        client_session_init_mock, determine_artist_id_mock, get_works_by_artist_id_mock,
        count_lyrics_for_work_mock, print_mock) -> None:
    client_session = mock.Mock(ClientSession)
    client_session_init_mock.return_value = _context_manager_yielding(client_session)

    artist_id = str(uuid.uuid4())
    determine_artist_id_mock.return_value = artist_id

    work_names = ['First Work', 'Second Work', 'Third Work']
    get_works_by_artist_id_mock.return_value = _async_generator_yielding(*work_names)

    count_lyrics_for_work_mock.side_effect = [5, None, 10]

    artist_name = 'My Favourite Artist'
    await print_average_lyric_count_for_artist(artist_name)
    print_mock.assert_called_once_with("Average lyric count: 7.5")

    determine_artist_id_mock.assert_called_once_with(client_session, artist_name)

    get_works_by_artist_id_mock.assert_called_once_with(client_session, artist_id)

    for work_name in work_names:
        count_lyrics_for_work_mock.assert_any_call(client_session, artist_name, work_name)


def _awaitable_value(value: Any) -> Future:
    future = Future()
    future.set_result(value)
    return future


async def _async_generator_yielding(*values: Any) -> Generator[Any, None, None]:
    for value in values:
        yield value


@asynccontextmanager
async def _context_manager_yielding(value: Any) -> Generator[Any, None, None]:
    yield value
