from contextlib import asynccontextmanager
from typing import Any, Generator
from unittest import mock

import pytest
from aiohttp import ClientResponse, ClientSession

from xeona.lyrical_statistics.musicbrainz import get_artists_by_name, get_works_for_artist


@pytest.mark.asyncio
async def test_get_lyrics_for_work_calls_correct_url_and_returns_successful_response_body_as_json() -> None:
    response_body_json = mock.Mock()

    response_mock = mock.Mock(ClientResponse)
    response_mock.status = 200
    response_mock.json.return_value = response_body_json

    client_session_mock = mock.Mock(ClientSession)
    client_session_mock.get.return_value = _context_manager_yielding(response_mock)

    assert await get_artists_by_name(client_session_mock, 'My favourite artist') is response_body_json
    client_session_mock.get.assert_called_once_with(
        "https://musicbrainz.org/ws/2/artist/",
        params={'query': 'name:My favourite artist'},
        headers={'Accept': 'application/json'})


@pytest.mark.asyncio
async def test_get_artists_by_name_raises_value_error_on_unsuccessful_response() -> None:
    response_mock = mock.Mock(ClientResponse)
    response_mock.status = 404

    client_session_mock = mock.Mock(ClientSession)
    client_session_mock.get.return_value = _context_manager_yielding(response_mock)

    with pytest.raises(ValueError):
        await get_artists_by_name(client_session_mock, 'My favourite artist')


@pytest.mark.asyncio
async def test_get_works_for_artist_calls_correct_url_and_returns_successful_response_body_as_json() -> None:
    response_body_json = mock.Mock()

    response_mock = mock.Mock(ClientResponse)
    response_mock.status = 200
    response_mock.json.return_value = response_body_json

    client_session_mock = mock.Mock(ClientSession)
    client_session_mock.get.return_value = _context_manager_yielding(response_mock)

    assert await get_works_for_artist(client_session_mock, 'my_favourite_artist_id') is response_body_json
    client_session_mock.get.assert_called_once_with(
        "https://musicbrainz.org/ws/2/work/",
        params={'artist': 'my_favourite_artist_id'},
        headers={'Accept': 'application/json'})


@pytest.mark.asyncio
async def test_get_works_for_artist_raises_value_error_on_unsuccessful_response() -> None:
    response_mock = mock.Mock(ClientResponse)
    response_mock.status = 404

    client_session_mock = mock.Mock(ClientSession)
    client_session_mock.get.return_value = _context_manager_yielding(response_mock)

    with pytest.raises(ValueError):
        await get_works_for_artist(client_session_mock, 'my_favourite_artist_id')


@asynccontextmanager
async def _context_manager_yielding(value: Any) -> Generator[Any, None, None]:
    yield value
