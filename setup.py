from setuptools import setup, find_namespace_packages

setup(
    name='xeona.lyrical-statistics',
    version="0.0.1",
    packages=find_namespace_packages(),
    python_requires='>=3.8',
    entry_points={
        'console_scripts': [
            'xeona.lyrical_statistics = xeona.lyrical_statistics.__main__:main',
        ]
    },
    install_requires=[
        'aiohttp',
    ],
    tests_require=[
        'pytest',
        'pytest-asyncio',
    ]
)
