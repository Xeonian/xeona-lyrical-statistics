import asyncio
import re

from argparse import ArgumentParser
from statistics import mean
from typing import Generator, Optional

from aiohttp import ClientSession

from xeona.lyrical_statistics import musicbrainz, lyrics_ovh


async def determine_artist_id(session: ClientSession, artist_name: str) -> str:
    artists_response = await musicbrainz.get_artists_by_name(session, artist_name)
    return artists_response['artists'][0]['id']


async def get_works_by_artist_id(session: ClientSession, artist_id: str) -> Generator[str, None, None]:
    work_count: Optional[int] = None
    offset: int = 0

    while work_count is None or work_count > offset:
        works_response = await musicbrainz.get_works_for_artist(session, artist_id, offset=offset)
        work_count = works_response['work-count']
        for work in works_response['works']:
            yield work['title']
            offset += 1


async def count_lyrics_for_work(session: ClientSession, artist_name: str, work_name: str) -> Optional[int]:
    try:
        lyrics_response = await lyrics_ovh.get_lyrics_for_work(session, artist_name, work_name)
        return len(re.findall(r'\S+', lyrics_response['lyrics']))
    except ValueError:
        return None


async def print_average_lyric_count_for_artist(artist_name: str) -> None:
    async with ClientSession() as session:
        artist_id = await determine_artist_id(session, artist_name)
        lyrics_per_work = await asyncio.gather(*[
            count_lyrics_for_work(session, artist_name, work_name)
            async for work_name in get_works_by_artist_id(session, artist_id)
        ])

    average_lyrics_per_work = mean([lyrics_for_work
                                    for lyrics_for_work in lyrics_per_work
                                    if lyrics_for_work is not None])

    print(f"Average lyric count: {average_lyrics_per_work}")


def main() -> None:
    argument_parser = ArgumentParser(
        description="Determines the average number of words per work for a requested artist")
    argument_parser.add_argument("artist_name", type=str, help="the name of the artist to request")

    arguments = argument_parser.parse_args()

    artist_name = arguments.artist_name
    loop = asyncio.get_event_loop()
    loop.run_until_complete(print_average_lyric_count_for_artist(artist_name))


if __name__ == '__main__':
    main()
