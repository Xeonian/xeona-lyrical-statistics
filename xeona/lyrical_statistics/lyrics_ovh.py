from http import HTTPStatus
from typing import Dict, Any

from aiohttp import ClientSession


async def get_lyrics_for_work(session: ClientSession, artist_name: str, work_name: str) -> Dict[str, Any]:
    async with session.get(f"https://api.lyrics.ovh/v1/{artist_name}/{work_name}") as response:
        if response.status == HTTPStatus.OK:
            return await response.json()

        # TODO: More meaningful error type
        raise ValueError

