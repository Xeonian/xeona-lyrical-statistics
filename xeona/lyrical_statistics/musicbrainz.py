from http import HTTPStatus
from typing import Any, Dict

from aiohttp import ClientSession

_API_ENDPOINT = "https://musicbrainz.org/ws/2/"


async def get_artists_by_name(session: ClientSession, artist_name: str) -> Dict[str, Any]:
    return await _make_api_request(session, f"artist/", query=f"name:{artist_name}")


async def get_works_for_artist(session: ClientSession, artist_id: str, *, limit: int = None, offset: int = None) \
        -> Dict[str, Any]:
    params = {'artist': artist_id}
    if limit is not None:
        params['limit'] = limit
    if offset is not None:
        params['offset'] = offset
    return await _make_api_request(session, f"work/", **params)


async def _make_api_request(session: ClientSession, path: str, **params: str) -> Dict[str, Any]:
    async with session.get(f"{_API_ENDPOINT}{path}",
                           params=params,
                           headers={'Accept': 'application/json'}) as response:
        if response.status == HTTPStatus.OK:
            return await response.json()

        # TODO: Different exception type
        raise ValueError
